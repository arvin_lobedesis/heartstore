<!DOCTYPE html>
<html>
<head>
	<title>HeartStore</title>
	<!-- bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/minty/bootstrap.css">
</head>
<body>
	<header>
		<!-- navbar for bootswatch -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <a class="navbar-brand" href="index.php">HeartStore</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarColor02">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="views/catalog.php">Products <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="views/add-item.php">Add Item</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="views/cart.php">Cart</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">About</a>
		      </li>
		    </ul>
		    <form class="form-inline my-2 my-lg-0">
		      <input class="form-control mr-sm-2" type="text" placeholder="Search">
		      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
		    </form>
		  </div>
		</nav>
		<div class="d-flex justify-content-center align-items-center flex-column" style="height: 90vh">
			<h1 class="text-secondary" style="font-size: 50px">Welcome to HeartStore</h1>
			<a href="views/catalog.php" class="btn btn-secondary">View Products</a>
		</div>
	</header>
	<!-- Feature products page -->
	<section>
		<h1 class="text-center p-5">Featured Items</h1>
		<div class="container">
			<div class="row">
				<?php 
					$products = file_get_contents("assets/lib/products.json");
					// var_dump($products);
					$products_array = json_decode($products, true);
					// var_dump($products_array);
					for($i=0;$i<3;$i++){
				?>
					<div class="col-lg-4 py-5">
						<div class="card">
							<img src="assets/lib/<?php echo($products_array[$i]["image"])?>" class="card-img-top" height="350px">
							<div class="card-body">
								<h5 class="card-title"><?php echo $products_array[$i]["name"] ?></h5>
								<p class="card-text">Price: Php<?php echo $products_array[$i]["price"] ?></p>
								<p class="card-text">Description: <?php echo $products_array[$i]["description"] ?></p>
							</div>
						</div>
					</div>
				<?php		
					}
				 ?>
			</div>
		</div>
	</section>

	<!-- footer -->
	<footer class="page-footer font-small bg-dark navbar-dark" >
		<div class="footer-copyright text-center py-3 text-dark">2020 HeartStore. All Rights reserved.</div>
	</footer>

</body>
</html>