<?php 
	require "../partials/template.php";
	function get_body_contents(){

 ?>
	<h1 class="text-center py-3">Add Item</h1>
	<div class="col-lg-6 offset-lg-3 py-5">
		<form action="../controllers/add-item-process.php" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label for="name">Product Name</label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group">
				<label for="price">Price:</label>
				<input type="number" name="price" class="form-control">
			</div>
			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="description" class="form-control" style="resize: none;" rows="3"></textarea>
			</div>
			<div class="form-group">
				<label for="image">Image:</label>
				<input type="file" name="image" class="form-control">
			</div>
			<button type="submit" class="btn btn-secondary">Add Item</button>
		</form>
	</div>
 <?php 

	}	

?>